# variant 1

@receiver(post_save,sender=Campaign,dispatch_uid="caimpaign_signal")
def caimpaign_signal(sender,**kwargs):
    instance = kwargs.get('instance')

    if (!instance in Campaign.objects.all()):
        print('Campaign deleted')
    elif !instance == sender:
        print('Campaign updated')
    else:
        print('Campaign created')

# variant 2

@receiver(post_save,sender=Campaign,dispatch_uid="caimpaign_signal")
def caimpaign_signal(sender,**kwargs):
    instance = kwargs.get('instance')

    if (!instance in Campaign.objects.all()):
        print('Campaign deleted')
    elif (!instance.__dict__ == sender.__dict__):
        print('Campaign updated')
    else:
        print('Campaign created')
