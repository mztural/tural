from django.db.models import Q
from django.db import models


class Salary(models.Model):
    amount = models.PositiveIntegerField()
    retired = models.BooleanField()

s = Salary(amount=500, retired=True)

s.save()

myfilter = Salary.objects.filter(Q(amount!=5300) & Q(retired=True))
