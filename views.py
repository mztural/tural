from rest_framework import permissions
from .serializers import *
from rest_framework import mixins

User = get_user_model()


class UserList(mixins.ListModelMixin, generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)

    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer

    serializers = {
        'default': UserSerializer,
        'update': UpdateUserSerializer,
    }

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
                user = User.objects.filter(email=request.POST.get('email',''))
                print(user[0])
                payload = jwt_payload_handler(user[0])
                token = jwt_encode_handler(payload)
                the_json = {'token': token}
                # return Response(json.dumps(the_json), status=status.HTTP_201_CREATED)
                return JsonResponse(the_json, status=201)
            except:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        serializer = UpdateUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.update(request.user, request.data)
            return Response(serializer.data, status=status.HTTP_205_RESET_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
