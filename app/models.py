from django.contrib.auth.models import AbstractUser
from django.db import models

GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )


class User(AbstractUser):
    address = models.CharField(max_length=255, verbose_name="address")
    firstName = models.CharField(max_length=255, verbose_name="firstname", default="ali")
    lastName = models.CharField(max_length=255, verbose_name="lastname", default="aliyev")
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default='M', null=True)
