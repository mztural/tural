from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserLoginSerializer(serializers.ModelSerializer):
    # password = serializers.CharField(write_only=True, required=False)
    class Meta:
        model = get_user_model()
        fields = ('email', 'password')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('height', 'date', 'email', 'gender', 'password')
