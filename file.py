import os


def print_directory_contents(sPath):
    dirs = os.listdir(sPath)
    minpath = sPath
    # This would print all the files and directories
    for file in dirs:
        try:
            apath = minpath
            minpath = minpath+"/"+file
            print_directory_contents(minpath)
            minpath = apath
            print file
        except:
            print file

print_directory_contents('/home/hadoop')
